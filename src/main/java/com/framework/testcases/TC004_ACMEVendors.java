package com.framework.testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.framework.design.ProjectMethods;
import com.framework.pages.ACMELoginPage;
import com.framework.pages.LoginPage;

public class TC004_ACMEVendors extends ProjectMethods{
	
	@BeforeTest
	public void setData() {
		testCaseName="TC004_ACMEVendors";
		testDescription="ACME Search Vendors";
		testNodes="Leads";
		author="Srikanth";
		category="smoke";
		dataSheetName="TC004";
	}
	@Test(dataProvider="fetchData")
	public void login(String userName, String password, String vendorid) {
		new ACMELoginPage()
		.enterUsername(userName)
		.enterPassword(password)
		.clickLogin()
		.hoverOverVendor()
		.clickSearchVendor()
		.enterVendorId(vendorid)
		.clickSearchButton()
		.dispVendorName();
	
		
		 
	}

}
