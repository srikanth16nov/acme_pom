package com.framework.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.framework.design.ProjectMethods;

public class ACMEVendorSearch extends ProjectMethods{

	public ACMEVendorSearch() {
       PageFactory.initElements(driver, this);
	}

	@FindBy(how = How.ID,using="vendorTaxID") WebElement eleVendorId;
	@FindBy(how=How.ID,using="buttonSearch") WebElement eleSearchLink;
	
	public ACMEVendorSearch enterVendorId(String data) {
		clearAndType(eleVendorId, data);
		return this;
	}
	
	public ACMESearchResult clickSearchButton() {
		click(eleSearchLink);
		return new ACMESearchResult();
	}
	
	
	
	
	
	


}
















