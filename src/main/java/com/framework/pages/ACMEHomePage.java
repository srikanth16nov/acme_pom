package com.framework.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.framework.design.ProjectMethods;

public class ACMEHomePage extends ProjectMethods{

	public ACMEHomePage() {
       PageFactory.initElements(driver, this);
	}

	@FindBy(how = How.XPATH,using="(//button[@class='btn btn-default btn-lg' ])[4]") WebElement eleVendor;
	@FindBy(how=How.LINK_TEXT,using="Search for Vendor") WebElement eleSearchLink;
	
	public ACMEHomePage hoverOverVendor() {
		mouseHoverOver(eleVendor);
		return this;
	}
	
	public ACMEVendorSearch clickSearchVendor() {
		mouseClick(eleSearchLink);
		return new ACMEVendorSearch();
	}
	
	/*public LoginPage clickSearchVendor() {
		//WebElement eleLogout = locateElement("class", "decorativeSubmit");
		Actions bv = new Actions(driver);
		bv.moveToElement(eleVendor).perform();
		bv.contextClick(eleSearchLink).perform();
		click(eleVendor);
		return new LoginPage(); 
	}*/
	
	


}
















