package com.framework.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.framework.design.ProjectMethods;

public class ACMELoginPage extends ProjectMethods{

	public ACMELoginPage() {
		//apply PageFactory
		PageFactory.initElements(driver, this); 
	}
	
	@FindBy(how = How.ID,using="email") WebElement eleUsername;
	@FindBy(how = How.ID,using="password") WebElement elePassword;
	@FindBy(how = How.ID,using="buttonLogin") WebElement eleLogin;
	
	public ACMELoginPage enterUsername(String data) {
		//WebElement eleUsername = locateElement("id", "username");
		clearAndType(eleUsername, data);
		return this; 
	}
	public ACMELoginPage enterPassword(String data) {
		//WebElement elePassword = locateElement("id", "password");
		clearAndType(elePassword, data);
		return this;
	}
	public ACMEHomePage clickLogin() {
		//WebElement eleLogin = locateElement("class", "decorativeSubmit");
	    click(eleLogin);
	    /*HomePage hp = new HomePage();
	    return hp;*/ 
	    return new ACMEHomePage();
	}
}













